import pandas as pd
import logging
from torch.utils.data import Dataset
import torch


logger = logging.getLogger()
logger.setLevel(logging.INFO)

# segment IDs for first and second example
SEQ1, SEQ2 = 0, 1
LABEL_ENCODER = {
    "entailment": 0,
    "neutral": 1,
    "contradiction": 2
}
IDX_TO_LABEL = {idx: label for label, idx in LABEL_ENCODER.items()}


class NLIDataset(Dataset):
    def __init__(self, example_ids, premises, hypotheses, labels, tokenizer, max_seq_len):
        # Note: set `labels` to None if there are no labels available
        self.ids = example_ids
        self.num_examples = len(premises)

        self.inputs, self.segments, self.attn_masks, self.labels = [], [], [], []
        for idx in range(self.num_examples):
            encoded = tokenizer.encode_plus(premises[idx], hypotheses[idx],
                                            max_length=max_seq_len, pad_to_max_length=True)

            self.inputs.append(encoded["input_ids"])
            self.segments.append(encoded["token_type_ids"])
            self.attn_masks.append(encoded["attention_mask"])
            if labels is None:
                self.labels.append([])
            else:
                self.labels.append(LABEL_ENCODER[labels[idx]])

        self.inputs = torch.tensor(self.inputs)
        self.segments = torch.tensor(self.segments)
        self.attn_masks = torch.tensor(self.attn_masks)
        self.labels = torch.tensor(self.labels)

    def __getitem__(self, index):
        return self.ids[index], self.inputs[index], self.segments[index], self.attn_masks[index], self.labels[index]

    def __len__(self):
        return self.num_examples


def load_nli(file_path, sample_size=None):
    """ Common loader for SNLI/MultiNLI """
    df = pd.read_csv(file_path, sep="\t", na_values=[""], nrows=sample_size, encoding="utf-8", quoting=3)
    # Drop examples where one of the sentences is "n/a"
    df = df.dropna(axis=0, how="any", subset=["sentence1", "sentence2"])
    mask = df["gold_label"] != "-"
    df = df.loc[mask].reset_index(drop=True)

    logging.info(f"Loaded {df.shape[0]} examples from set '{file_path}'")
    return df


