import os
from data import NLIDataset
from transformers import BertTokenizer
from predictor import BertNLIPredictor
from data import IDX_TO_LABEL
import pandas as pd
import json

if __name__ == "__main__":
    DATA_DIR = "data/QNLI"
    dev = os.environ.get("QNLI_DEV_PATH", os.path.join(DATA_DIR, "dev.tsv"))

    MAX_SEQ_LEN = 82
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    df_dev = pd.read_csv(dev, sep="\t", quoting=3)
    df_dev = df_dev.loc[df_dev["label"] == "entailment"]

    dev_r2l = NLIDataset(example_ids=df_dev["index"].values,
                         premises=df_dev["sentence"].values,
                         hypotheses=df_dev["question"].values,
                         labels=None,
                         tokenizer=tokenizer,
                         max_seq_len=MAX_SEQ_LEN)

    controller = BertNLIPredictor(pretrained_model_name_or_path="models/qnli_bert_base_uncased_b24_lr6.25e-05_maxseqlen82",
                                  batch_size=24)
    preds = controller.predict(dev_r2l).cpu().numpy()

    paraphrase_indices = []
    for i, predicted_lbl in enumerate(preds):
        str_label = IDX_TO_LABEL[predicted_lbl]
        if str_label == "entailment":
            paraphrase_indices.append(i)

    formatted = []
    for idx, question, sent in df_dev.iloc[paraphrase_indices][["index", "question", "sentence"]].values:
        formatted.append({
            "index": idx,
            "question": question,
            "sentence": sent
        })

    with open("qnli_extracted_paras.json", "w", encoding="utf-8") as f:
        json.dump(formatted, f, indent=4)

