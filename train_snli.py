import os

from transformers import BertTokenizer

from data import NLIDataset, load_nli
from trainer import BertNLITrainer

if __name__ == "__main__":
    DATA_DIR = "data/snli"
    train_path = os.environ.get("SNLI_TRAIN_PATH", os.path.join(DATA_DIR, "snli_1.0_train.txt"))
    val_path = os.environ.get("SNLI_VAL_PATH", os.path.join(DATA_DIR, "snli_1.0_dev.txt"))

    MAX_SEQ_LEN = 41
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

    df_train = load_nli(train_path)
    df_val = load_nli(val_path)
    train_dataset = NLIDataset(example_ids=df_train["pairID"].values,
                               premises=df_train["sentence1"].values,
                               hypotheses=df_train["sentence2"].values,
                               labels=df_train["gold_label"].values,
                               tokenizer=tokenizer,
                               max_seq_len=MAX_SEQ_LEN)
    val_dataset = NLIDataset(example_ids=df_val["pairID"].values,
                             premises=df_val["sentence1"].values,
                             hypotheses=df_val["sentence2"].values,
                             labels=df_val["gold_label"].values,
                             tokenizer=tokenizer,
                             max_seq_len=MAX_SEQ_LEN)

    BATCH_SIZE, LR = 24, 6.25e-5
    controller = BertNLITrainer(model_name=f"snli_bert_base_uncased_b{BATCH_SIZE}_lr{LR}_maxseqlen{MAX_SEQ_LEN}",
                                pretrained_model_name_or_path="bert-base-uncased",
                                batch_size=BATCH_SIZE,
                                lr=LR)
    controller.run(train_dataset, num_epochs=5, val_dataset=val_dataset)
