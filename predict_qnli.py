import os
from data import NLIDataset
from transformers import BertTokenizer
from predictor import BertNLIPredictor
from sklearn.metrics import f1_score, accuracy_score
from data import IDX_TO_LABEL
import numpy as np
import pandas as pd

if __name__ == "__main__":
    DATA_DIR = "data/QNLI"
    test_path = os.environ.get("QNLI_TEST_PATH", os.path.join(DATA_DIR, "dev.tsv"))

    MAX_SEQ_LEN = 82
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    df_test = pd.read_csv(test_path, sep="\t", quoting=3)

    test_l2r = NLIDataset(example_ids=df_test["index"].values,
                          premises=df_test["question"].values,
                          hypotheses=df_test["sentence"].values,
                          labels=None,
                          tokenizer=tokenizer,
                          max_seq_len=MAX_SEQ_LEN)

    controller = BertNLIPredictor(pretrained_model_name_or_path="models/qnli_bert_base_uncased_b24_lr6.25e-05_maxseqlen82",
                                  batch_size=24)
    preds = controller.predict(test_l2r).cpu().numpy()

    binary_mapping = {
        "entailment": 0,
        "not_entailment": 1
    }

    converted_preds = []
    for predicted_lbl in preds:
        str_label = IDX_TO_LABEL[predicted_lbl]
        if str_label == "entailment":
            converted_preds.append(binary_mapping["entailment"])
        else:
            converted_preds.append(binary_mapping["not_entailment"])
    converted_preds = np.array(converted_preds)
    targets = df_test["label"].apply(lambda i: binary_mapping[i]).values

    print("QNLI dev scores:")
    print(f"Accuracy: {accuracy_score(targets, converted_preds)}")
    print(f"F1 score (avg): {f1_score(targets, converted_preds, average='binary')}")
