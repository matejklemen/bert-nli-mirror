import os
from data import NLIDataset
from transformers import BertTokenizer
from predictor import BertNLIPredictor
import pandas as pd
import numpy as np
from data import LABEL_ENCODER, IDX_TO_LABEL
import json

if __name__ == "__main__":
    DATA_DIR = "data/rtv_nli"
    test_path = os.environ.get("RTV_TEST_PATH", os.path.join(DATA_DIR, "rtv_nli_dev.csv"))

    MAX_SEQ_LEN = 193
    tokenizer = BertTokenizer.from_pretrained("bert-base-multilingual-uncased")

    df_test = pd.read_csv(test_path)
    test_l2r = NLIDataset(example_ids=np.arange(df_test.shape[0]),
                          premises=df_test["sent1"].values,
                          hypotheses=df_test["sent2"].values,
                          labels=None,
                          tokenizer=tokenizer,
                          max_seq_len=MAX_SEQ_LEN)
    test_r2l = NLIDataset(example_ids=np.arange(df_test.shape[0]),
                          premises=df_test["sent2"].values,
                          hypotheses=df_test["sent1"].values,
                          labels=None,
                          tokenizer=tokenizer,
                          max_seq_len=MAX_SEQ_LEN)

    controller = BertNLIPredictor(pretrained_model_name_or_path="models/rtv_nli_bert_base_multilingual_uncased_b24_lr6.25e-05_maxseqlen193",
                                  batch_size=24)
    preds_l2r = controller.predict(test_l2r).cpu().numpy()
    preds_r2l = controller.predict(test_r2l).cpu().numpy()

    preds_para = np.logical_and(preds_l2r == LABEL_ENCODER["entailment"],
                                preds_r2l == LABEL_ENCODER["entailment"]).astype(np.int32)

    formatted = []
    for i in range(df_test.shape[0]):
        formatted.append({
            "id": int(i),
            "sent1": df_test.iloc[i]["sent1"],
            "sent2": df_test.iloc[i]["sent2"],
            "l2r": IDX_TO_LABEL[preds_l2r[i]],
            "r2l": IDX_TO_LABEL[preds_r2l[i]],
            "predicted": int(preds_para[i]),
            "actual_l2r": df_test.iloc[i]["gold_label"]
        })

    pred_filename = "rtv_nli_dev_preds.json"
    with open(pred_filename, "w") as f:
        print(f"Dumping predictions to {pred_filename}")
        json.dump(formatted, f, indent=4)
