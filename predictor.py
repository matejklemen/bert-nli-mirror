from transformers import BertForSequenceClassification
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
from time import time
import logging

DEVICE = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
logging.info(f"Using device {DEVICE}")


class BertNLIPredictor:
    def __init__(self, pretrained_model_name_or_path, batch_size=24):
        self.batch_size = batch_size
        self.model = BertForSequenceClassification.from_pretrained(pretrained_model_name_or_path).to(DEVICE)

    def _process_batch(self, inputs, segments, attn_masks):
        """ Returns label probabilities for a single batch (shape: [batch_size, num_labels]) """
        logits = self.model(inputs, token_type_ids=segments, attention_mask=attn_masks)[0]
        return F.softmax(logits, dim=1)

    def predict_proba(self, test_dataset):
        """
        Arguments
        ---------
        test_dataset: NLIDataset
            Test data, containing at least encoded inputs, segments (token types) and attention masks for each example

        Returns
        -------
        torch.tensor
            Predicted probabilities for examples (shape: [batch_size, num_labels])
        """
        with torch.no_grad():
            self.model.eval()
            proba_preds = []
            pred_start = time()
            for _, curr_inputs, curr_segments, curr_masks, _ in DataLoader(test_dataset, shuffle=False,
                                                                           batch_size=self.batch_size):
                proba_preds.append(self._process_batch(curr_inputs.to(DEVICE),
                                                       curr_segments.to(DEVICE),
                                                       curr_masks.to(DEVICE)))

            logging.info(f"Prediction took {time() - pred_start: .4f}s")
            return torch.cat(proba_preds)

    def predict(self, test_dataset):
        """ Convenience function which sets the prediction as the most probable label (i.e. argmax over probas). """
        probas = self.predict_proba(test_dataset)
        return torch.argmax(probas, dim=1)
