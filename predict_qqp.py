import os
from data import NLIDataset
from transformers import BertTokenizer
from predictor import BertNLIPredictor
from sklearn.metrics import f1_score, accuracy_score
from data import LABEL_ENCODER, IDX_TO_LABEL
import numpy as np
import pandas as pd
from time import gmtime, strftime
import json

if __name__ == "__main__":
    DATA_DIR = "data/qqp"
    test_path = os.environ.get("QQP_TEST_PATH", os.path.join(DATA_DIR, "dev.tsv"))

    MAX_SEQ_LEN = 41
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

    df_test = pd.read_csv(test_path, sep="\t")
    test_l2r = NLIDataset(example_ids=df_test["id"].values,
                          premises=df_test["question1"].values,
                          hypotheses=df_test["question2"].values,
                          labels=None,
                          tokenizer=tokenizer,
                          max_seq_len=MAX_SEQ_LEN)
    test_r2l = NLIDataset(example_ids=df_test["id"].values,
                          premises=df_test["question2"].values,
                          hypotheses=df_test["question1"].values,
                          labels=None,
                          tokenizer=tokenizer,
                          max_seq_len=MAX_SEQ_LEN)

    USE_MODEL = os.environ.get("USE_MODEL", "models/snli_bert_base_uncased_b24_lr6.25e-05_maxseqlen41")
    controller = BertNLIPredictor(pretrained_model_name_or_path=USE_MODEL,
                                  batch_size=24)
    preds_l2r = controller.predict(test_l2r).cpu().numpy()
    preds_r2l = controller.predict(test_r2l).cpu().numpy()

    preds_para = np.logical_and(preds_l2r == LABEL_ENCODER["entailment"],
                                preds_r2l == LABEL_ENCODER["entailment"]).astype(np.int32)

    has_label = "is_duplicate" in df_test.columns
    formatted = []
    for i in range(df_test.shape[0]):
        formatted.append({
            "id": int(df_test.iloc[i]["id"]),
            "question1": df_test.iloc[i]["question1"],
            "question2": df_test.iloc[i]["question2"],
            "l2r": IDX_TO_LABEL[preds_l2r[i]],
            "r2l": IDX_TO_LABEL[preds_r2l[i]],
            "predicted": int(preds_para[i]),
            "is_duplicate": int(df_test.iloc[i]['is_duplicate']) if has_label else "N/A"
        })

    pred_filename = f"preds_{strftime('%Y-%m-%d %H:%M:%S', gmtime())}.json"
    with open(pred_filename, "w") as f:
        print(f"Dumping predictions to {pred_filename}")
        json.dump(formatted, f, indent=4)

    if has_label:
        targets = df_test["is_duplicate"].values.astype(np.int32)
        print("QQP scores:")
        print(f"Accuracy: {accuracy_score(targets, preds_para)}")
        print(f"F1 score: {f1_score(targets, preds_para, average='binary')}")
