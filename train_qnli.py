import os
import pandas as pd

from transformers import BertTokenizer

from data import NLIDataset
from trainer import BertNLITrainer

if __name__ == "__main__":
    DATA_DIR = "data/QNLI"
    train_path = os.environ.get("QNLI_TRAIN_PATH", os.path.join(DATA_DIR, "train.tsv"))
    val_path = os.environ.get("QNLI_VAL_PATH", os.path.join(DATA_DIR, "dev.tsv"))

    MAX_SEQ_LEN = 82
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

    df_train = pd.read_csv(train_path, sep="\t", quoting=3)
    df_val = pd.read_csv(val_path, sep="\t", quoting=3)

    # Mark "not_entailment" as "neutral" so that we can use standard NLI framework
    df_train.at[df_train["label"] == "not_entailment", "label"] = "neutral"
    df_val.at[df_val["label"] == "not_entailment", "label"] = "neutral"

    train_dataset = NLIDataset(example_ids=df_train["index"].values,
                               premises=df_train["question"].values,
                               hypotheses=df_train["sentence"].values,
                               labels=df_train["label"].values,
                               tokenizer=tokenizer,
                               max_seq_len=MAX_SEQ_LEN)
    val_dataset = NLIDataset(example_ids=df_val["index"].values,
                             premises=df_val["question"].values,
                             hypotheses=df_val["sentence"].values,
                             labels=df_val["label"].values,
                             tokenizer=tokenizer,
                             max_seq_len=MAX_SEQ_LEN)

    BATCH_SIZE, LR = 24, 6.25e-5
    controller = BertNLITrainer(model_name=f"qnli_bert_base_uncased_b{BATCH_SIZE}_lr{LR}_maxseqlen{MAX_SEQ_LEN}",
                                pretrained_model_name_or_path="bert-base-uncased",
                                batch_size=24,
                                lr=LR)
    controller.run(train_dataset, num_epochs=5, val_dataset=val_dataset)
