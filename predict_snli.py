import os
from data import NLIDataset, load_nli
from transformers import BertTokenizer
from predictor import BertNLIPredictor
from sklearn.metrics import f1_score, accuracy_score
from data import LABEL_ENCODER
import numpy as np

if __name__ == "__main__":
    DATA_DIR = "data/snli"
    test_path = os.environ.get("SNLI_TEST_PATH", os.path.join(DATA_DIR, "snli_1.0_test.txt"))

    MAX_SEQ_LEN = 41
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

    df_test = load_nli(test_path)
    test_dataset = NLIDataset(example_ids=df_test["pairID"].values,
                              premises=df_test["sentence1"].values,
                              hypotheses=df_test["sentence2"].values,
                              labels=df_test["gold_label"].values,
                              tokenizer=tokenizer,
                              max_seq_len=MAX_SEQ_LEN)

    controller = BertNLIPredictor(pretrained_model_name_or_path="models/snli_bert_base_uncased_b24_lr6.25e-05_maxseqlen41",
                                  batch_size=24)
    preds = controller.predict(test_dataset).cpu().numpy()
    targets = np.array([LABEL_ENCODER[lbl] for lbl in df_test["gold_label"].values])

    print("SNLI test scores:")
    print(f"Accuracy: {accuracy_score(targets, preds)}")
    print(f"F1 score (micro): {f1_score(targets, preds, average='micro')}")
    print(f"F1 score (macro): {f1_score(targets, preds, average='macro')}")
