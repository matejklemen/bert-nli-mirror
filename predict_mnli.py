import os
from data import NLIDataset, load_nli
from transformers import BertTokenizer
from predictor import BertNLIPredictor

if __name__ == "__main__":
    DATA_DIR = "data/multinli"
    test_path = os.environ.get("MNLI_TEST_PATH",os.path.join(DATA_DIR, "multinli_1.0_dev_matched.txt"))

    MAX_SEQ_LEN = 73
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

    df_test = load_nli(test_path)
    test_dataset = NLIDataset(example_ids=df_test["pairID"].values,
                              premises=df_test["sentence1"].values,
                              hypotheses=df_test["sentence2"].values,
                              labels=df_test["gold_label"].values,
                              tokenizer=tokenizer,
                              max_seq_len=MAX_SEQ_LEN)

    controller = BertNLIPredictor(pretrained_model_name_or_path="models/joze", batch_size=24)
    pred_probas = controller.predict_proba(test_dataset)
