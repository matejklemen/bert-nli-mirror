import logging
import os

import torch
import torch.optim as optim
from torch.utils.data import Subset, DataLoader
from transformers import BertForSequenceClassification, BertConfig
from time import time

from data import LABEL_ENCODER

MODELS_SAVE_DIR = "models"
DEVICE = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
logging.info(f"Using device {DEVICE}")


class BertNLITrainer:
    def __init__(self, model_name, pretrained_model_name_or_path, batch_size=24, lr=6.25e-5,
                 validate_every_n_steps=5_000, early_stopping_tol=5):
        self.model_save_path = os.path.join(MODELS_SAVE_DIR, model_name)
        if not os.path.exists(self.model_save_path):
            os.makedirs(self.model_save_path)

        self.batch_size = batch_size
        self.lr = lr
        self.validate_every_n_steps = validate_every_n_steps
        self.early_stopping_tol = early_stopping_tol

        self.config = BertConfig.from_pretrained(pretrained_model_name_or_path)
        self.config.num_labels = len(LABEL_ENCODER)

        self.model = BertForSequenceClassification.from_pretrained(pretrained_model_name_or_path,
                                                                   config=self.config).to(DEVICE)
        self.optimizer = optim.AdamW(self.model.parameters(), lr=self.lr)

    def _process_batch(self, inputs, segments, attn_masks, labels, eval_mode=False):
        """ Processes a single batch for training/validation. """
        output = self.model(inputs, token_type_ids=segments, attention_mask=attn_masks, labels=labels)
        loss, logits = output[:2]

        if not eval_mode:
            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

        return float(loss), logits

    def train(self, train_dataset):
        self.model.train()
        train_loss = 0.0
        for _, curr_inputs, curr_segments, curr_masks, curr_labels in DataLoader(train_dataset, shuffle=False,
                                                                                 batch_size=self.batch_size):
            curr_loss, _ = self._process_batch(curr_inputs.to(DEVICE),
                                               curr_segments.to(DEVICE),
                                               curr_masks.to(DEVICE),
                                               curr_labels.to(DEVICE))
            train_loss += curr_loss

        return train_loss

    def validate(self, val_dataset):
        with torch.no_grad():
            val_loss = 0.0
            self.model.eval()
            for _, curr_inputs, curr_segments, curr_masks, curr_labels in DataLoader(val_dataset, shuffle=False,
                                                                                     batch_size=self.batch_size):
                curr_loss, _ = self._process_batch(curr_inputs.to(DEVICE),
                                                   curr_segments.to(DEVICE),
                                                   curr_masks.to(DEVICE),
                                                   curr_labels.to(DEVICE),
                                                   eval_mode=True)
                val_loss += curr_loss

            return val_loss

    def run(self, train_dataset, num_epochs, val_dataset=None):
        best_val_loss, no_increase = float("inf"), 0
        stop_early = False

        train_start = time()
        for idx_epoch in range(num_epochs):
            logging.info(f"Epoch {1 + idx_epoch}/{num_epochs}")
            shuffled_indices = torch.randperm(len(train_dataset))

            num_minisets = (len(train_dataset) + self.validate_every_n_steps - 1) // self.validate_every_n_steps
            for idx_miniset in range(num_minisets):
                logging.info(f"Miniset {1 + idx_miniset}/{num_minisets}")
                curr_subset = Subset(train_dataset, shuffled_indices[idx_miniset * self.validate_every_n_steps:
                                                                     (idx_miniset + 1) * self.validate_every_n_steps])
                num_sub_batches = (len(curr_subset) + self.batch_size - 1) // self.batch_size
                train_loss = self.train(curr_subset) / num_sub_batches
                logging.info(f"Training loss = {train_loss: .4f}")

                if val_dataset is None or len(curr_subset) < self.validate_every_n_steps // 2:
                    logging.info(f"Skipping validation after training on a small training subset "
                                 f"({len(curr_subset)} < {self.validate_every_n_steps // 2} examples)")
                    continue

                num_val_batches = (len(val_dataset) + self.batch_size - 1) // self.batch_size
                val_loss = self.validate(val_dataset) / num_val_batches
                logging.info(f"Validation loss = {val_loss: .4f}")
                if val_loss < best_val_loss:
                    logging.info("New best! Saving checkpoint")
                    best_val_loss = val_loss
                    no_increase = 0
                    self.save()
                else:
                    no_increase += 1

                if no_increase == self.early_stopping_tol:
                    logging.info(f"Stopping early after validation loss did not improve for "
                                 f"{self.early_stopping_tol} rounds")
                    stop_early = True
                    break

            if stop_early:
                break

        logging.info(f"Training took {time() - train_start:.4f}s")

    def save(self):
        self.model.save_pretrained(self.model_save_path)

