import os
from transformers import BertTokenizer

from data import NLIDataset
from trainer import BertNLITrainer
import pandas as pd
import numpy as np

if __name__ == "__main__":
    DATA_DIR = "data/rtv_nli"
    train_path = os.environ.get("RTV_TRAIN_PATH", os.path.join(DATA_DIR, "rtv_nli_train.csv"))
    val_path = os.environ.get("RTV_VAL_PATH", os.path.join(DATA_DIR, "rtv_nli_dev.csv"))

    MAX_SEQ_LEN = 193
    tokenizer = BertTokenizer.from_pretrained("bert-base-multilingual-uncased")

    df_train = pd.read_csv(train_path)
    df_val = pd.read_csv(val_path)

    train_dataset = NLIDataset(example_ids=np.arange(df_train.shape[0]),
                               premises=df_train["sent1"].values,
                               hypotheses=df_train["sent2"].values,
                               labels=df_train["gold_label"].values,
                               tokenizer=tokenizer,
                               max_seq_len=MAX_SEQ_LEN)
    val_dataset = NLIDataset(example_ids=np.arange(df_train.shape[0], df_train.shape[0] + df_val.shape[0]),
                             premises=df_val["sent1"].values,
                             hypotheses=df_val["sent2"].values,
                             labels=df_val["gold_label"].values,
                             tokenizer=tokenizer,
                             max_seq_len=MAX_SEQ_LEN)

    BATCH_SIZE, LR = 24, 6.25e-5
    controller = BertNLITrainer(model_name=f"rtv_nli_bert_base_multilingual_uncased_b{BATCH_SIZE}_lr{LR}_maxseqlen{MAX_SEQ_LEN}",
                                pretrained_model_name_or_path="bert-base-multilingual-uncased",
                                batch_size=24,
                                lr=LR)
    controller.run(train_dataset, num_epochs=5, val_dataset=val_dataset)

